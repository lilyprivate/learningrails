class Article < ActiveRecord::Base
  attr_accessible :text, :title
  validates :title, presence: true,
            length: {minimum: 5}

  def update_values(dict)
    self.text = dict[:text]
    self.title = dict[:title]
    save!
  end
end
