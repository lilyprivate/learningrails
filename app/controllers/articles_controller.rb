class ArticlesController < ApplicationController

  def index
    @articles = Article.all
  end
  
  def new
    @article = Article.new
  end

  def create
    #render plain: params[:article].inspect
    #render plain: params[:article].inspect

    @article = Article.new(params[:article])

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
    #puts "\n" * 20
    #puts @article.to_hash
    #redirect_to '/articles/show'
    
  end

  def show
    @article = Article.find(params[:id])
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update_values(params[:article])
      redirect_to @article
    else
      render 'edit'
    end
  end
  
end
